autoplay = {}
autoplay.autoplay_on_players = {} -- p_name = bool
autoplay.set_timer = 15 -- check for queueing every this many seconds
autoplay.timer = 0
autoplay.enabled = true






minetest.register_privilege("autoplay", "Enable/disable Autoplay globally")



minetest.register_chatcommand("autoplay", {
    privs = {
        autoplay = true,
    },
    func = function(name, param)
        if not param then
            return false, "You must specify 'enable' or disable!"
        end
        if param == "enable" then
            autoplay.enabled = true
            return true, "Autoplay has been enabled globally!"
        end
        if param == "disable" then
            autoplay.enabled = false
            return true, "Autoplay has been disabled globally!"
        end

    end,
})





--===================    get_available arenas   =====================
--===================================================================
--   Input: num_players (int) how many players are available
--   Output:  (table) that contains mod names as indexies and a table of arena ids as the value. Can return an empty table of length 0 if no arenas are enabled

function autoplay.get_available_arenas( num_players )

    local available_arenas_table = {} -- {mod = {arena_id,arena_id,arena_id,}, mod = {arena_id,arena_id,arena_id,},}
    local mods = arena_lib.mods
    ----minetest.log( "mods table:".. string.sub(dump(mods),1,2000 ))
    for mod,stats in pairs(mods) do
        
        local include_mod = false
        local available_arena_ids = {}
        local arenas = stats.arenas
        --minetest.log( "arenas table:".. string.sub(dump(arenas),1,2000 ) )

        if arenas then
            --minetest.log( "length of arenas table:".. #arenas )  
            
            for arena_ID,arena in ipairs(arenas) do     
                local min_players = arena.min_players
                local max_players = arena.max_players

                if arena.teams_enabled then
                    multi = #arena.teams
                    min_players = min_players * multi
                    max_players = max_players * multi
                end

                --debug
                --minetest.log( "arena:"..arena.name )     
                --minetest.log( "enabled:" ..tostring(arena.enabled))
                --minetest.log( "num_players:"..num_players )
                --minetest.log( "min_players" ..min_players )
                --minetest.log( "max_players"..max_players )
                
                if arena.enabled and 
                  num_players >= min_players and 
                  num_players ~= max_players + 1 and
                  not( arena.in_queue ) and
                  not( arena.in_loading) and
                  not( arena.in_game ) and
                  not( arena.in_celebration ) then --if the arena is enabled 
                    -- and there are more players available than the minimum required for the arena 
                    -- and we wont leave just one player in the lobby if we sign everyone else up, then list the arena as available.
                    include_mod = true
                    table.insert( available_arena_ids , arena_ID)
                end
            end

        end


        if include_mod and available_arena_ids then

            --insert the arena ids into the available arenas table at mod index
            --minetest.log( "available_arena_ids: \n".. string.sub(dump(available_arena_ids),1,2000 ) )  
            available_arenas_table[mod] = available_arena_ids
        end


    end
    --minetest.log( "Available_arenas_table: ".. dump(available_arenas_table) )  
    return available_arenas_table    

end




















minetest.register_globalstep(function(dtime)

    if not autoplay.enabled then return end

    autoplay.timer = autoplay.timer + dtime

    if not (autoplay.timer > autoplay.set_timer) then return end --dont do anything if timer not reached

    autoplay.timer = 0 --reset the timer

    local available_players = {}

    for p_name, avail in pairs(autoplay.autoplay_on_players) do
        if avail and not(arena_lib.is_player_in_arena(p_name)) and 
            not(arena_lib.is_player_spectating(p_name)) and 
            not(arena_lib.is_player_in_queue(p_name)) and 
            not(arena_lib.is_player_in_edit_mode(p_name)) then
              
              
            if not(minetest.get_modpath("parties")) or (minetest.get_modpath("parties") and not(parties.is_player_in_party(p_name))) then
              table.insert(available_players, p_name)
            end
            ----minetest.log( "player "..p_name.."was inserted into available players")
        end
    end
    ----minetest.log( "length of available players is:" .. #available_players)
    if #available_players <= 1 then return end --dont do anything if there are only 0 or 1 available players

    local queue_again = true -- used to enter leftover players into another game if needed

    while queue_again do

        local available_arenas = autoplay.get_available_arenas(#available_players)

        ----minetest.log( "length of available arenas is:" .. #available_arenas)

        if not (available_arenas) then return end

        --choose a random available mod
        local tbl = {}
        for mod,_ in pairs(available_arenas) do
            table.insert(tbl,mod)
        end
        --minetest.log("tbl: \n"..dump(tbl))
        local rand = math.random(1,#tbl)
        --minetest.log("rand: "..rand)
        local chosen_mod = tbl[rand] --its a string, modname. also the idx in available_arenas
        --choose a random available arena
        local arena_list = available_arenas[chosen_mod]
        --minetest.log("available_arenas: \n"..dump(available_arenas))
        --minetest.log("chosen_mod: "..chosen_mod)
        --minetest.log("Arena_list: \n"..dump(arena_list))
        local chosen_arena_id = arena_list[math.random(1,#arena_list)]
        local chosen_arena_name = arena_lib.mods[chosen_mod].arenas[chosen_arena_id].name
        local arena_max_players = arena_lib.mods[chosen_mod].arenas[chosen_arena_id].max_players
        
        



        for i = 1, arena_max_players do

            if #available_players > 0 then

                local pl_idx = math.random(1,#available_players)
                local chosen_p_name = available_players[pl_idx]
                local sign_pos = arena_lib.mods[chosen_mod].arenas[chosen_arena_id].sign
                if sign_pos then

                    if minetest.forceload_block(sign_pos) then

                        --local node = minetest.get_node(sign_pos)
                        local puncher = minetest.get_player_by_name(chosen_p_name)
                        if puncher then

                          arena_lib.queue_player_at_sign(sign_pos, puncher)
                          table.remove( available_players , pl_idx )
                          minetest.chat_send_player(chosen_p_name, minetest.colorize("#e6482e","[Autoplay] The chosen Game is: "..chosen_mod ..", \nThe chosen Arena is: "..chosen_arena_name))
        
                        end

                    end

                end

                if #available_players == 0 then --dont choose another minigame for leftover players and let everyone else know that what game is chosen, if we have finished assigning players
                    queue_again = false
                    for _, t_player in pairs(minetest.get_connected_players()) do
                        local pl_name = t_player:get_player_name()
                        if not(arena_lib.is_player_in_arena(pl_name)) and 
                        not(arena_lib.is_player_spectating(pl_name)) and 
                        not(arena_lib.is_player_in_queue(pl_name)) and 
                        not(arena_lib.is_player_in_edit_mode(pl_name)) then

                            if not(minetest.get_modpath("parties")) or (minetest.get_modpath("parties") and not(parties.is_player_in_party(pl_name))) then

                                minetest.chat_send_player(pl_name, minetest.colorize("#394778","[Autoplay] The Autoplay Game is: "..chosen_mod ..", \nThe chosen Arena is: "..chosen_arena_name ))
                        
                            end
                        end

                    end
                end
            end
        end
    end


end)

minetest.register_craftitem("autoplay:toggle", {

    description = "Toggle Autoplay",
    inventory_image = "autoplay_toggle.png",
    on_drop = function(itemstack, dropper, pos) return itemstack end,
    on_use = function(itemstack, user, pointed_thing)
        if user and user:is_player() then
            local p_name = user:get_player_name()
            if not(arena_lib.is_player_in_arena(p_name)) and 
            not(arena_lib.is_player_spectating(p_name)) and 
            not(arena_lib.is_player_in_edit_mode(p_name)) then 
                if minetest.get_modpath("parties") then
                    if parties.is_player_in_party(p_name) then
                        autoplay.autoplay_on_players[p_name] = false
                        minetest.chat_send_player(p_name,minetest.colorize("#a93b3b","[Autoplay] You can't enable Autoplay while in a party!"))
                        return
                    end
                end
                if autoplay.autoplay_on_players[p_name] ~= nil then
                    if autoplay.autoplay_on_players[p_name] == true then
                        autoplay.autoplay_on_players[p_name] = false
                        minetest.chat_send_player(p_name,minetest.colorize("#a93b3b","[Autoplay] Your Autoplay has been disabled"))
                    else
                        autoplay.autoplay_on_players[p_name] = true
                        minetest.chat_send_player(p_name,minetest.colorize("#397b44","[Autoplay] Your Autoplay has been Enabled"))
                    end
                    if autoplay.enabled == false then
                        minetest.chat_send_player(p_name,minetest.colorize("#a93b3b","[Autoplay] Note: Autoplay is disabled on the server at this time!"))
                    end
                end
            end
        end
    
    end,
})

minetest.register_on_joinplayer(function(player, last_login)
    autoplay.autoplay_on_players[player:get_player_name()] = false
end)

minetest.register_on_leaveplayer(function(player, timed_out)
    local p_name = player:get_player_name()
    if not p_name then return end
    autoplay.autoplay_on_players[p_name] = nil
end)


if minetest.get_modpath("parties") then

    -- if autoplay.autoplay_on_players[p_name] == true then
    --     minetest.chat_send_player(sender, minetest.colorize("#e6482e", S("[!] You can't perform this action while in queue!")))
    --     return false end



    parties.register_on_pre_party_join(function(party_leader, p_name)
        
        if autoplay.autoplay_on_players[p_name] == true then
            autoplay.autoplay_on_players[p_name] = false
            minetest.chat_send_player(p_name,minetest.colorize("#a93b3b","[Autoplay] Your Autoplay has been disabled"))
        end
        return true
    
    end)

end

    